<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'nemphuonganh' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
define('ALLOW_UNFILTERED_UPLOADS', true);
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' xsx_I<Zz7a*HFl=#Fv*?{j<Kgt-s;Whz!A#Pn!KqKf~Xg+-{2ydjX493ZZJZpWX' );
define( 'SECURE_AUTH_KEY',  'Q=C*YG>$NZ4>?-ZUq$vBbKF_RZo4N:GpXh3f9I)b1n6Un%p.scA,+JWUGW>U-^[0' );
define( 'LOGGED_IN_KEY',    '$q-YR:{=T]jFF}h !xBA)epNxv|y=6DgeL4k3R80 a>7EGqR`GmS4xMAaf?S7wiA' );
define( 'NONCE_KEY',        'i~f]`IDg}Cm9)MiX0GI),x_8?8gvD6 e6ui62 zcdQP_^58(2L}b vKWhws6ka;v' );
define( 'AUTH_SALT',        '*a:0kSg=ApPcz15^^}#?y)$M)Mu>D2d{@0&;#~_<&-x9LR_+bYrQ 7R7y9NUn~BK' );
define( 'SECURE_AUTH_SALT', '}@g+AE+que ZjyyI8_+cy<_i8V63QO]oxW M>3Ma8^%`s02FdctI:Axd(s`x,tz<' );
define( 'LOGGED_IN_SALT',   '^ddv-A.t{!uLDPqDCT#|)_=SOU5)z@I&,l:^oAi#g[ss:&o9u5ebp#8-DHz0eP&i' );
define( 'NONCE_SALT',       '=.B !J6[324vu>_`[r_n4m!:jj[B-ULQ 3CP2rguFb8>R9Q8/XPolZ~/:-]S1g.K' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';